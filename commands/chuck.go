package commands

import (
	"net/http"
	"encoding/json"
)

func GetChuck() string{

	r, err := http.Get("https://api.chucknorris.io/jokes/random")
	if err != nil {
		return "ey"
	}
	defer r.Body.Close()

	type Chuck struct {
		Value string `json:"value"`
	}

	chuck := new(Chuck)

	json.NewDecoder(r.Body).Decode(chuck)

	return chuck.Value
}