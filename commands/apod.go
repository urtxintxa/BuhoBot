package commands

import (
	"log"
	"strings"
	"github.com/PuerkitoBio/goquery"
)

func GetApod(date string) (string, string, bool){

	uri := "https://apod.nasa.gov/apod/ap"+dateToAposDate(date)+".html"

	doc, err := goquery.NewDocument(uri)
	if err != nil {
		log.Fatal(err)
	}

	url := []string{}
	doc.Find("a").Each(func(i int, sc *goquery.Selection) {
		sc.Find("IMG").Each(func(i int, s *goquery.Selection) {
			a, _ := sc.Attr("href")
			url = append(url, a)
		})
	})

	if(len(url) == 0) {
		return 	"There is no image content on "+date+". (Maybe video or javascript) Checkout the link for more information." ,
			uri, false
	} else {
		return "https://apod.nasa.gov/apod/" + url[0], uri, true
	}
}

func dateToAposDate(date string) (string) {
	date = date[2:]
	date = strings.Replace(date, "/", "", -1)
	return date
}
