package commands

import (
	"net/http"
	"encoding/json"
	"strconv"
)

func GetPokedex(id string) (string, string){

	uri := "http://pokeapi.co/api/v2/pokemon/" + id

	r, err := http.Get(uri)
	if err != nil {
		return "Bad id number or name", ""
	}
	defer r.Body.Close()

	type Pokemon struct {
		Name	string `json:"name"`
		ID	float64 `json:"id"`
	}

	data := new(Pokemon)

	json.NewDecoder(r.Body).Decode(data)

	if(data.Name == "") {
		return "Bad id number or name", ""
	}

	//return strconv.FormatFloat(data.ID, 'f', -1, 64)
	return 	"Name: " + data.Name + " ID: " + strconv.FormatFloat(data.ID, 'f', -1, 64),
		"https://img.pokemondb.net/artwork/"+data.Name+".jpg"

}
