package commands

import (
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
)

func GetCyanHappRcg() string {
	return getImageUrl("http://explosm.net/rcg")
}

func GetCyanHapp() string {
	resp, err := http.Get("http://explosm.net/comics/random")
	if err != nil {
		log.Fatalf("http.Get => %v", err.Error())
	}

	randomUrl := resp.Request.URL.String()
	return getImageUrl(randomUrl)

}

func getImageUrl(url string) string{
	doc, err := goquery.NewDocument(url)
	if err != nil {
		log.Fatal(err)
	}

	urls := []string{}
	doc.Find("meta").Each(func(i int, s *goquery.Selection) {
		x, _ := s.Attr("property")
		if(x == "og:image") {
			a, _ := s.Attr("content")
			urls = append(urls, a)
		}
	})

	return urls[0]
}