package commands

import (
	"github.com/PuerkitoBio/goquery"
	"log"
)

func GetAbs() string {
	doc, err := goquery.NewDocument("http://abstrusegoose.com/pseudorandom.php")
	if err != nil {
		log.Fatal(err)
	}

	url := []string{}

	doc.Find("meta").Each(func(i int, s *goquery.Selection) {
		a, _ := s.Attr("content")
		url = append(url, a)

	})

	uri := url[1]
	uriNew := uri[6:]

	doc, err = goquery.NewDocument(uriNew)
	if err != nil {
		log.Fatal(err)
	}

	url = []string{}
	doc.Find("section").Each(func(i int, sc *goquery.Selection) {
		sc.Find("img").Each(func(i int, s *goquery.Selection) {
			a, _ := s.Attr("src")
			url = append(url, a)
		})
	})

	return url[0]
}
