package commands

import (
	"net/http"
	"log"
	"github.com/PuerkitoBio/goquery"
)

func GetGag() (string, bool) {
	resp, err := http.Get("http://9gag.com/random")
	if err != nil {
		log.Fatalf("http.Get => %v", err.Error())
	}

	randomUrl := resp.Request.URL.String()

	doc, err := goquery.NewDocument(randomUrl)
	if err != nil {
		log.Fatal(err)
	}

	url := []string{}

	doc.Find("source").Each(func(i int, s *goquery.Selection) {
		a, _ := s.Attr("type")
		if a == "video/mp4" {
			b, _ := s.Attr("src")
			url = append(url, b)
		}

	})

	if len(url) != 0 {
		return url[0], true // Video
	} else {
		url = []string{}
		doc.Find("a").Each(func(i int, s *goquery.Selection) {
			a, _ := s.Attr("data-img")
			if a != "" {
				url = append(url, a)
			}
		})
	}
	return url[0], false // Image
}
