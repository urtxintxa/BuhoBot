package commands

import (
	"github.com/PuerkitoBio/goquery"
	"log"
)

func GetPhd() string {
	doc, err := goquery.NewDocument("http://www.phdcomics.com/comics/archive.php?random=1")
	if err != nil {
		log.Fatal(err)
	}

	url := []string{}
	doc.Find("img").Each(func(i int, s *goquery.Selection) {
		x, _ := s.Attr("name")
		if(x == "comic") {
			a, _ := s.Attr("src")
			url = append(url, a)
		}
	})

	return url[0]
}