package commands

import (
	"github.com/PuerkitoBio/goquery"
	"log"
)

func GetTdhd() string {
	doc, err := goquery.NewDocument("http://thedoghousediaries.com/")
	if err != nil {
		log.Fatal(err)
	}

	url := []string{}

	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		x, _ := s.Attr("href")
		url = append(url, x)

	})

	doc, err = goquery.NewDocument(url[0])
	if err != nil {
		log.Fatal(err)
	}

	url = []string{}
	doc.Find("meta").Each(func(i int, s *goquery.Selection) {
		x, _ := s.Attr("property")
		if (x == "og:image") {
			a, _ := s.Attr("content")
			url = append(url, a)
		}
	})

	return url[0]
}