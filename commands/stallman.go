package commands

import (
	"github.com/PuerkitoBio/goquery"
	"log"
)

func GetStallman() string{

	uri := "https://rms.sexy/"

	doc, err := goquery.NewDocument(uri)
	if err != nil {
		log.Fatal(err)
	}

	url := []string{}
	doc.Find("img").Each(func(i int, s *goquery.Selection) {
		x, _ := s.Attr("class")
		if(x == "stallman") {
			a, _ := s.Attr("src")
			url = append(url, a)
		}
	})

	return uri + url[0]
}
