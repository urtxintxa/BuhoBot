package commands

import (
	"encoding/json"
	"net/http"
)

func GetCat() string {

	r, err := http.Get("http://random.cat/meow")
	if err != nil {
		return "ey"
	}
	defer r.Body.Close()

	type Cat struct {
		File string `json:file`
	}

	data := new(Cat)

	json.NewDecoder(r.Body).Decode(data)

	return data.File
}
