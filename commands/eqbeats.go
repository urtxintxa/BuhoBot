package commands

import (
	"time"
	"net/http"
	"strconv"
	"encoding/json"
	"math/rand"
)

func GetEqbeats() string {

	rand.Seed(time.Now().Unix())
	post := rand.Intn(6358 - 1) + 1

	r, err := http.Get("https://eqbeats.org/tracks/all/json?per_page=1&page=" + strconv.Itoa(post))
	if err != nil {
		return ""
	}
	defer r.Body.Close()

	type Download struct{
		Mp3 string `json:"mp3"`
	}
	type Eq []struct{
		Song Download `json:"Download"`
	}

	decoder := json.NewDecoder(r.Body)
	var data Eq

	err = decoder.Decode(&data)
	if err != nil {
		return ""
	}
	return (data[0].Song.Mp3)
}
