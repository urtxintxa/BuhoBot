package commands

import (
	"github.com/PuerkitoBio/goquery"
	"strings"
	"log"
)

func GetPokemonFusion() (string, string){
	doc, err := goquery.NewDocument("http://pokemon.alexonsager.net/")
	if err != nil {
		log.Fatal(err)
	}

	poke1 := []string{}
	poke2 := []string{}
	doc.Find("img").Each(func(i int, s *goquery.Selection) {
		x, _ := s.Attr("id")
		if(x == "pic1") {
			a, _ := s.Attr("src")
			poke1 = append(poke1, a)
		} else if (x == "pic2") {
			a, _ := s.Attr("src")
			poke2 = append(poke2, a)
		}
	})

	poke1s := strings.TrimLeft(poke1[0], "http://images.alexonsager.net/pokemon/")
	poke2s := strings.TrimLeft(poke2[0], "http://images.alexonsager.net/pokemon/")

	poke1s = strings.TrimRight(poke1s, ".png") // First pokemon number
	poke2s = strings.TrimRight(poke2s, ".png") // Second pokemon number

	doc.Find("option").Each(func(i int, s *goquery.Selection) {
		x, _ := s.Attr("value")
		if(x == poke1s) {
			a := s.Text()
			poke1 = append(poke1, a)
		} else if (x ==  poke2s) {
			a:= s.Text()
			poke2 = append(poke2, a)
		}
	})

	image := []string{}
	doc.Find("img").Each(func(i int, s *goquery.Selection) {
		x, _ := s.Attr("id")
		if(x == "pk_img") {
			a, _ := s.Attr("src")
			image = append(image, a)
		}
	})

	return (poke1[1] + " + " + poke2[1] + " = " + poke1[3] + poke2[4]) , (image[0])
}