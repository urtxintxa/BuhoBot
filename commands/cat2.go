package commands

import (
	"net/http"
	"log"
)

func GetCat2() string{ // Getting random post number

	resp, err := http.Get("http://thecatapi.com/api/images/get.php")
	if err != nil {
		log.Fatalf("http.Get => %v", err.Error())
	}

	return resp.Request.URL.String()
}
