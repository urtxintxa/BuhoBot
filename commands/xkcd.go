package commands

import (
	"net/http"
	"encoding/json"
	"log"
)

func getXkdcPost() string{ // Getting random post number

	resp, err := http.Get("http://c.xkcd.com/random/comic/")
	if err != nil {
		log.Fatalf("http.Get => %v", err.Error())
	}

	return resp.Request.URL.String()
}

func GetXkdc() string{
	url := "http://xkcd.com" + getXkdcPost()[15:] + "info.0.json"

	r, err := http.Get(url)
	if err != nil {
		return "ey"
	}
	defer r.Body.Close()

	type Xkdc struct {
		Img string `json:"img"`
	}

	xkdc := new(Xkdc)

	json.NewDecoder(r.Body).Decode(xkdc)

	return xkdc.Img
}