package main

import (
	"github.com/jajasuperman/BuhoBot/commands"
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"regexp"
	"strings"
)

/* FOR @BOTFATHER
cat - Cat images
cat2 - Moar cat images
chuck - Chuck Norris funny jokes
xkcd - Comics from xkcd
eqbeats - Random song from eqbeats
9gag - Random image from 9gag (only from the 6 new ones)
abs - Comics from abstrusegoose
cyanide - Cyanide & Hapiness comics
cyanidercg - Cyanide & Hapiness comics (Random Comic Generator)
phd - Comics from phdcomics
tdhd- Comics from thedoghousediaries
*/

func main() {
	// New Bot
	bot, err := tgbotapi.NewBotAPI("129237987:AAH4LsxVlUrL1tBbis18bTJzORwgHzL6DT8")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true // Debug yes

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		commandHandler(bot, &update)
	}
}

func commandHandler(bot *tgbotapi.BotAPI, update *tgbotapi.Update) {
	if update.Message == nil {
		return
	}

	command := update.Message.Command()
	argc := update.Message.CommandArguments()
	chat := update.Message.Chat.ID // Get the chat id to send the images and text to that chat

	// Commands
	switch command {
	case "cat":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetCat())
		bot.Send(msg)
	case "cat2":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetCat2())
		bot.Send(msg)
	case "chuck":
		msg := tgbotapi.NewMessage(chat, commands.GetChuck())
		bot.Send(msg)
	case "xkcd":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetXkdc())
		bot.Send(msg)
	case "eqbeats":
		msg := tgbotapi.NewAudioShare(chat, commands.GetEqbeats())
		bot.Send(msg)
	case "9gag":
		fil , ext := commands.GetGag()
		if ext {
			bot.Send(tgbotapi.NewVideoShare(chat, fil))
		} else {
			bot.Send(tgbotapi.NewPhotoShare(chat, fil))
		}
	/*case "pokemonFusion":
		pok , img := commands.GetPokemonFusion()
		msg := tgbotapi.NewMessage(chat, pok)
		bot.Send(msg)
		msg1 := tgbotapi.NewPhotoShare(chat, "https://images.alexonsager.net/pokemon/133.png")
		bot.Send(msg1)*/
	case "pokedex":
		pok , img := commands.GetPokedex(argc)
		if (img != "") {
			msg := tgbotapi.NewPhotoShare(chat, img)
			msg.Caption = pok
			bot.Send(msg)
		} else {
			bot.Send(tgbotapi.NewMessage(chat, pok))
		}
	case "apod":
		rr, _ := regexp.MatchString("[0-9]{4}/[0-9]{2}/[0-9]{2}", argc) // Check if argument is like YYYY/MM/DD

		if (rr) {
			img , uri, isImg := commands.GetApod(argc)

			if(isImg) {
				msg := tgbotapi.NewPhotoShare(chat, img)
				msg.Caption = uri
				bot.Send(msg)
			}else {
				msg := tgbotapi.NewMessage(chat, img + " " + uri)
				bot.Send(msg)
			}
		} else {
			bot.Send(tgbotapi.NewMessage(chat, "Wrong param. Param must be YYYY/MM/DD. Example /apod 2016/10/20"))
		}
	case "identicon":
		if(argc != "") {
			url := "http://identicon.rmhdev.net/"+argc+".png"
			url = strings.Replace(url, " ", "%20", -1)
			log.Printf(url)
			msg := tgbotapi.NewPhotoShare(chat, url)
			msg.Caption = "Here is \"" + argc + "\" as identicon"
			bot.Send(msg)
		}else {
			bot.Send(tgbotapi.NewMessage(chat, "Wrong param. Param must be a text. Example /identicon buhobot"))
		}

	case "abs":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetAbs())
		bot.Send(msg)
	case "stallman":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetStallman())
		bot.Send(msg)
	case "cyanide":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetCyanHapp())
		bot.Send(msg)
	case "cyanidercg":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetCyanHappRcg())
		bot.Send(msg)
	case "phd":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetPhd())
		bot.Send(msg)
	case "tdhd":
		msg := tgbotapi.NewPhotoShare(chat, commands.GetTdhd())
		bot.Send(msg)
	default:
		log.Printf("No command")
	}
}
