# BuhoBot
A simple bot in Go using [Telegram Bot API](http://gopkg.in/telegram-bot-api.v4). 

Bot Commands
-
<table>
  <thead>
    <tr>
      <td><strong>Command</strong></td>
      <td><strong>Description</strong></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>/9gag</td>
      <td>Random images from http://9gag.com</td>
    </tr>
    <tr>
      <td>/abs</td>
      <td>Random images from http://abstrusegoose.com</td>
    </tr>    
    <tr>
      <td>/cat</td>
      <td>Random cat images from http://random.cat</td>
    </tr>
    <tr>
      <td>/cat2</td>
      <td>Random cat images from http://thecatapi.com</td>
    </tr>
    <tr>
      <td>/chuck</td>
      <td>Random Chuck Norris from http://api.chucknorris.io</td>
    </tr>   
    <tr>
      <td>/cyanhapp</td>
      <td>Random Cyanide & Happiness images from http://explosm.net</td>
    </tr>     
    <tr>
      <td>/cyanidercg</td>
      <td>Random Cyanide & Happiness images from http://explosm.net/rgc</td>
    </tr>        
    <tr>
      <td>/eqbeats</td>
      <td>Get random songs from http://eqbeats.org</td>
    </tr>      
    <tr>
      <td>/phd</td>
      <td>Get random images from http://www.phdcomics.com</td>
    </tr>  
    <tr>
      <td>/tdhd</td>
      <td>Get random images from http://thedoghousediaries.com</td>
    </tr>   
    <tr>
      <td>/xkcd</td>
      <td>Get random comics from http://xkcd.com</td>
    </tr>        
  </tbody>
</table>

Usage
-
Change TOKEN with your bot token. ([@botfather](https://telegram.me/BotFather) will give you the token)
```go
bot, err := tgbotapi.NewBotAPI(TOKEN)
````
To import the commands:
```go
go get github.com/jajasuperman/BuhoBot/commands
````
